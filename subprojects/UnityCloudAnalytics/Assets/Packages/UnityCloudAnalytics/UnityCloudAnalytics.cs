﻿using System.Collections.Generic;
using interfaces;
using UnityEngine.Cloud.Analytics;

namespace implementations
{
	public class UnityCloudAnalytics : IAnalytics
	{
		//it is sad :(
		private readonly Dictionary<string, object> _emptyDictionary = new Dictionary<string, object>();

		public void Init(List<string> initializationInfo)
		{
			UnityAnalytics.StartSDK(initializationInfo[0]);
		}

		public void StartSession()
		{
		}

		public void EndSession()
		{
		}

		//https://analytics.cloud.unity3d.com/docs#custom
		public void Log(string EventName, bool isTimed = false)
		{
			UnityAnalytics.CustomEvent(EventName, _emptyDictionary);
		}

		public void Log(string EventName, Dictionary<string, string> parameters)
		{
			var unityFriendlyDictionary = new Dictionary<string, object>();
			foreach (var kvp in parameters)
			{
				unityFriendlyDictionary.Add(kvp.Key, kvp.Value);
			}
			UnityAnalytics.CustomEvent(EventName, unityFriendlyDictionary);
		}
	}
}