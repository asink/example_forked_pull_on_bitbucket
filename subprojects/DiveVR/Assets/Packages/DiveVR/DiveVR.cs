﻿using System;
using System.Collections.Generic;
using interfaces;
using UnityEngine;

namespace implementations
{
	[Implements(typeof (IVR))]
	public class DiveVR : IVR
	{
		private readonly CameraLoader _loader;
		private readonly List<CameraCommands> _supportedCommands = new List<CameraCommands>();

		private readonly Dictionary<VRComponent, Transform> _vrTransforms = new Dictionary<VRComponent, Transform>
		{
			{VRComponent.LEFT_EYE, null},
			{VRComponent.RIGHT_EYE, null}
		};

		private Camera _leftCamera;
		private Camera _rightCamera;

		public DiveVR()
		{
			_loader = new CameraLoader("DiveVR");
		}

		public GameObject CreateCamera()
		{
			return _loader.CreateCamera();
		}

		public CameraSettingsSO GetCameraSettingsSO()
		{
			return _loader.CameraSettings;
		}

		public void CameraCommand(CameraCommands command)
		{
		}

		public void SetCustomCameraSettings(string settingString)
		{
		}

		public List<CameraCommands> GetSupportedCommands()
		{
			return _supportedCommands;
		}

		public Vector3 Position(VRComponent component)
		{
			if (component == VRComponent.CENTER)
			{
				var left = getComponent(VRComponent.LEFT_EYE).position;
				return left + (getComponent(VRComponent.RIGHT_EYE).position - left)/2f;
			}
			return getComponent(component).position;
		}

		public Quaternion Rotation(VRComponent component)
		{
			if (component == VRComponent.CENTER)
			{
				var left = getComponent(VRComponent.LEFT_EYE).rotation;
				return left;
			}
			return getComponent(component).rotation;
		}

		private Transform getComponent(VRComponent component)
		{
			if (_vrTransforms[component] != null) return _vrTransforms[component];
			Transform newTransform = null;

			findObjectCustom gameObjectFinder =
				(string childName) => { return _loader.CameraRef.transform.FindChild(childName); };
			switch (component)
			{
				case VRComponent.LEFT_EYE:
					newTransform = gameObjectFinder("Camera_left");
					break;
				case VRComponent.RIGHT_EYE:
					newTransform = gameObjectFinder("Camera_right");
					break;
				//case VRComponent.CENTER:
				//	newTransform = gameObjectFinder("");
				default:
					throw new NotSupportedException("tried to get an unrecognized vr position:" + component);
			}
			_vrTransforms[component] = newTransform;
			return newTransform;
		}

		public Transform leftCamera_LEGACY_HACK()
		{
			return getComponent(VRComponent.LEFT_EYE).transform;
		}

		public Transform rightCamera_LEGACY_HACK()
		{
			return getComponent(VRComponent.RIGHT_EYE).transform;
		}

		private delegate Transform findObjectCustom(string childName);
	}
}