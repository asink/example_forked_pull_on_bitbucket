﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using interfaces;
using UnityEditorCopies;

public class UseLevelLoaderService : MonoBehaviour, IRoutineRunner
{
	[SerializeField]
	public string levelName = "sampleleveleexport";
	[SerializeField]
	public string bundleName = "sampleleveleexport";

	public BuildTarget_Copy buildTarget;

	// Use this for initialization
	void Start()
	{
		StartCoroutine(startLevelLoader());
	}
	IEnumerator startLevelLoader()
	{
		yield return null;

		var bundleLoader = new LoadAssetBundleService()
		{
			analytics = new NullAnalytics(),
			contextParent = this.gameObject,
			logger = new UnityLogger(),
			model = new AssetBundleModel()
			{
				AssetbundleToModelList = new Dictionary<string, AssetBundleMetadata>()
			},
			platform = buildTarget.ToString(),
			runner = this
		};
		var loader = new LoadLevelService()
		{
			AssetBundleService = bundleLoader,
			logger = new UnityLogger(),
			model = bundleLoader.model,
			runner = this
		};
		yield return StartCoroutine(bundleLoader.LoadAssetBundle(levelName, BUNDLE_TYPE.LEVEL));
		yield return null;
		Debug.Log("After bundle, before loevel");
		yield return StartCoroutine(loader.LoadLevel(levelName, BUNDLE_TYPE.LEVEL));
		Debug.Log("After level load");
		yield return null;

	}
}
