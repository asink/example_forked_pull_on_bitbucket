﻿using System.Collections;
using System.IO;
using UnityEngine;

public class LoadTestAssetBundle : MonoBehaviour
{
	[SerializeField] private string _nameOfBundle = "StreamingAssets";
	//NOTE: StreamignAssets and test1 manifests/bundles get created.  only the "test1" bundle neess to be loaded -- I am unsure of the quality of StreamingAssets
	//after that -- there are actually 2 levels bundled in my initial tests -- only the sub-assets of the *first* level are loaded, the rest are not loaded in scene 2
	//the upshot is this -- still one level per bundle, unlesss you are *purely* remixing data that is already loaded in the main scene(different settings, but not assets)
	[SerializeField] private string _nameOfLevelToLoadAfter = "default";
	// Use this for initialization
	private void Start()
	{
		StartCoroutine(loadMe());
	}

	private IEnumerator loadMe()
	{
		var fullPath = Application.streamingAssetsPath + Path.DirectorySeparatorChar + _nameOfBundle;
		fullPath = fullPath.Replace("/", "\\");
		Debug.Log("Calllign www on :" + fullPath);
		var www = new WWW("file://" + fullPath);
		yield return www;
		Debug.Log(www.url + " and error :"+www.error);

		var objects = www.assetBundle.LoadAllAssets();
		Debug.Log("All assets:");
		foreach (var o in objects)
		{
			Debug.Log("object o:"+o.name + " assets:");
		}
		Application.LoadLevel(_nameOfLevelToLoadAfter);
		Debug.Log("Done loading level");
	}

}