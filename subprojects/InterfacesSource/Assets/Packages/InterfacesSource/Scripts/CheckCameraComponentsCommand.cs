﻿using interfaces;
using strange.extensions.command.impl;
using UnityEngine;

namespace implementations
{
	public class CheckCameraComponentsCommand : Command
	{
		[Inject]
		public IVR vr { get; set; }

		public override void Execute()
		{
			Debug.Log(string.Format("left pos {0} right pos {1}", vr.leftCamera_LEGACY_HACK().transform.position,
				vr.rightCamera_LEGACY_HACK().transform.position));
		}
	}
}