﻿using implementations;
using strange.extensions.context.impl;
using UnityEngine;

namespace interfaces
{
	public class SampleContext : HelloWorldContext
	{
		public SampleContext(MonoBehaviour contextView, bool autostart,InterfaceMappingUtilities mapUtilities)
			: base(contextView, autostart, mapUtilities)
		{
		}
		public SampleContext(MonoBehaviour contextView, bool autostart)
			: base(contextView, autostart)
		{
		}

		protected override void mapBindings()
		{
			base.mapBindings();
			if (Context.firstContext == this)
			{
				injectionBinder.Bind<string>()
				.ToName(NamedInjectionsCore.LOGFILE_ENDPOINT)
				.ToValue("https://techiealex.com/lifecoach/saveLog.php");
			}

			commandBinder.Bind<StartSignal>()
			.To<LogClientInfoCommand>().To<SendAllLogFilesCommand>().To<CheckCameraComponentsCommand>();
			//.To<LoadMainLevelCommand>();
		}
	}
}