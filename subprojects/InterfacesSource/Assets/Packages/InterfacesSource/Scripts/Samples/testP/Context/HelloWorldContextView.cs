﻿using System.Collections;
using strange.extensions.context.impl;
using UnityEngine;

namespace interfaces
{
	public class HelloWorldContextView : ContextView, IRoutineRunner
	{
		[SerializeField] protected InterfaceMappingUtilities utils;

		protected void Start()
		{
			utils.SetFrameRate();
			StartCoroutine(LoadHelloWorldLoadContext());
		}

		protected IEnumerator LoadHelloWorldLoadContext()
		{
			yield return StartCoroutine(utils.LoadInterfaceMapFromNet(this));
			context = new HelloWorldContext(this, true, utils);
			context.Start();
		}
	}
}