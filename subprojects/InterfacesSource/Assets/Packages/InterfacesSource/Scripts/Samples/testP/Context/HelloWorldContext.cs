﻿using UnityEngine;

namespace interfaces
{
	public class HelloWorldContext : SignalContext
	{
		protected InterfaceMappingUtilities _mappingUtilities;
		public HelloWorldContext(MonoBehaviour contextView, bool autostart, InterfaceMappingUtilities mappingUtilities)
			: base(contextView, autostart)
		{
			_mappingUtilities = mappingUtilities;
		}
		public HelloWorldContext(MonoBehaviour contextView, bool autostart)
			: base(contextView, autostart)
		{
			_mappingUtilities = new InterfaceMappingUtilities();
			_mappingUtilities.InterfaceMap = _mappingUtilities.GetDefaultMap();
		}
		protected override void mapBindings()
		{
			base.mapBindings();
			
			ContextUtilities contextUtil = new ContextUtilities(_mappingUtilities.InterfaceMap, injectionBinder);

			contextUtil.standardInitialization();
		}

		protected override void postBindings()
		{
			//if(Context.firstContext == this)
			//{
			//Debug.Log("PostBindings");
			base.postBindings();
		}
	}
}