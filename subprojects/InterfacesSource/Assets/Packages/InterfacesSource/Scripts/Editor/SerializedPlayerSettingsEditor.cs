﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorCopies;
using UnityEngine;
using Object = UnityEngine.Object;

namespace interfaces
{
	//todo: inheret and extend?
	public class SerializedPlayerSettingsEditor : SerializedPlayerSettings
	{
		//NOTE: if this throws an error, we have an invalid config
		public T guidToAsset<T>(string guid) where T : Object
		{
			var asset = AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(guid), typeof (T)) as T;
			return asset;
		}

		public static T getEnumFromCompatibleOne<T>(string input) //where T : Enum
		{
			//return (UnityEditor.UIOrientation)Enum.Parse(typeof(UnityEditor.UIOrientation), defaultInterfaceOrientation.ToString())
			return (T) Enum.Parse(typeof (T), input);
		}

		public void GetFromStaticInstance(BuildTarget_Copy buildTargetCopy, BuildTargetGroup_Copy currentBuildTargetGroupCopyInput)
		{
			var currentBuildTargetGroup =
				getEnumFromCompatibleOne<UnityEditor.BuildTargetGroup>(currentBuildTargetGroupCopyInput.ToString());

			bundleIdentifier = PlayerSettings.bundleIdentifier;
			//bundleVersion = PlayerSettings.bundleVersion;
			//PlayerSettings.targetGlesGraphics;
			companyName = PlayerSettings.companyName;
			defaultInterfaceOrientation = (UIOrientation_Copy) PlayerSettings.defaultInterfaceOrientation;
			defaultIsFullScreen = PlayerSettings.defaultIsFullScreen;
			defaultScreenHeight = PlayerSettings.defaultScreenHeight;
			defaultScreenWidth = PlayerSettings.defaultScreenWidth;
			displayResolutionDialog = (ResolutionDialogSetting_Copy) PlayerSettings.displayResolutionDialog;
			supportedAspectRatios = new List<AspectRatio_Copy>();
			resizableWindow = PlayerSettings.resizableWindow;
			foreach (UnityEditor.AspectRatio ratio in Enum.GetValues(typeof (UnityEditor.AspectRatio)))
			{
				if (supportedAspectRatios == null) break;
				if (PlayerSettings.HasAspectRatio(ratio))
					supportedAspectRatios.Add((AspectRatio_Copy) ratio);
			}
			productName = PlayerSettings.productName;
			RenderingPathCopy = (RenderingPath_Copy) PlayerSettings.renderingPath;
			scriptingDefineSymbols = new List<string>();
			scriptingDefineSymbols.Add(
				PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup));
			stripUnusedMeshComponents = PlayerSettings.stripUnusedMeshComponents;
			use32BitDisplayBuffer = PlayerSettings.use32BitDisplayBuffer;

			var optionalScriptingBackend = 0;
			PlayerSettings.GetPropertyOptionalInt("ScriptingBackend", ref optionalScriptingBackend, currentBuildTargetGroup);
			scriptingBackend = (ScriptingImplementation_Copy) optionalScriptingBackend;

			var optionalArch = 0;
			PlayerSettings.GetPropertyOptionalInt("Architecture", ref optionalArch, currentBuildTargetGroup);
			iPhoneArchitecture = (iPhoneArchitecture_Copy) optionalArch;
			
			splashScreenGUIDAkaResolutionDialogBanner =
				AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(PlayerSettings.resolutionDialogBanner));

			graphicsAPIs =
				getGraphicsListFromGraphicsAPIs(getEnumFromCompatibleOne<UnityEditor.BuildTarget>(buildTargetCopy.ToString())).ToArray();

			usePlayerLog = PlayerSettings.usePlayerLog;
			//Write a log file with debugging information --reccomended to turn off in prod
			defaultIconGUID =
				AssetDatabase.AssetPathToGUID(
					AssetDatabase.GetAssetPath(PlayerSettings.GetIconsForTargetGroup(UnityEditor.BuildTargetGroup.Unknown)[0]));
			var iconsTmp = PlayerSettings.GetIconsForTargetGroup(currentBuildTargetGroup);
			if (iconsTmp != null)
			{
				iconGroupGUIDs = new string[iconsTmp.Length];
				for (var i = 0; i < iconsTmp.Length; i++)
				{
					Object icon = iconsTmp[i];
					iconGroupGUIDs[i] = "";
					try
					{
						iconGroupGUIDs[i] = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(icon));
					}
					catch (Exception e)
					{
						Debug.LogError("Error when trying to get asset path for an icon --" + e);
					}
				}
			}

			virtualRealitySupported = PlayerSettings.virtualRealitySupported;
		}

		public void SetStaticInstanceFromThis(BuildTarget_Copy buildTargetCopy,
			BuildTargetGroup_Copy buildTargetGroupCopyIamBuildingAgainstInput)
		{
			var buildTargetGroupIAMBuildingAgainst =
				getEnumFromCompatibleOne<UnityEditor.BuildTargetGroup>(buildTargetGroupCopyIamBuildingAgainstInput.ToString());

			PlayerSettings.bundleIdentifier = bundleIdentifier;
			//PlayerSettings.bundleVersion = bundleVersion;
			PlayerSettings.companyName = companyName;
			PlayerSettings.defaultInterfaceOrientation =
				getEnumFromCompatibleOne<UnityEditor.UIOrientation>(defaultInterfaceOrientation.ToString());
			PlayerSettings.defaultIsFullScreen = defaultIsFullScreen;
			PlayerSettings.defaultScreenHeight = defaultScreenHeight;
			PlayerSettings.defaultScreenWidth = defaultScreenWidth;
			PlayerSettings.displayResolutionDialog =
				getEnumFromCompatibleOne<UnityEditor.ResolutionDialogSetting>(displayResolutionDialog.ToString());
			PlayerSettings.resizableWindow = resizableWindow;
			if (!string.IsNullOrEmpty(defaultIconGUID))
			{
				var defaultIcon =
					AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(defaultIconGUID), typeof (Texture2D)) as Texture2D;
				PlayerSettings.SetIconsForTargetGroup(UnityEditor.BuildTargetGroup.Unknown, new[] {defaultIcon});
			}
			// PlayerSettings.GetIconsForTargetGroup(buildTargetGroupIAMBuildingAgainst)
			if (iconGroupGUIDs != null)
			{
				//var icongroupCurrentSettings = PlayerSettings.GetIconsForTargetGroup(buildTargetGroupIAMBuildingAgainst);
				var textures = new Texture2D[iconGroupGUIDs.Length];
				//foreach(var textureGUID in iconGroupGUIDs)
				for (var i = 0; i < iconGroupGUIDs.Length; i++)
				{
					var textureGUID = iconGroupGUIDs[i];
					if (string.IsNullOrEmpty(textureGUID))
					{
						continue;
					}
					var tex =
						AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(textureGUID), typeof (Texture2D)) as Texture2D;
					if (tex == null)
					{
						Debug.LogError("Could not find asset path of texture:" + textureGUID);
						continue;
					}
					textures[i] = tex;
				}

				PlayerSettings.SetIconsForTargetGroup(buildTargetGroupIAMBuildingAgainst, textures);
			}

			//set them all to false, then expliccitly set the ones added to true
			foreach (UnityEditor.AspectRatio ratio in Enum.GetValues(typeof (UnityEditor.AspectRatio)))
			{
				PlayerSettings.SetAspectRatio(ratio, false);
			}
			foreach (var supportedAspectRatio in supportedAspectRatios)
			{
				var unityAspectRatio = getEnumFromCompatibleOne<UnityEditor.AspectRatio>(supportedAspectRatio.ToString());
				PlayerSettings.SetAspectRatio(unityAspectRatio, true);
			}
			setGraphicsAPIFromCopiesList(getEnumFromCompatibleOne<UnityEditor.BuildTarget>(buildTargetCopy.ToString()));

			PlayerSettings.productName = productName;
			PlayerSettings.renderingPath = getEnumFromCompatibleOne<UnityEngine.RenderingPath>(RenderingPathCopy.ToString());
			PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroupIAMBuildingAgainst,
				(scriptingDefineSymbols != null) ? scriptingDefineSymbols.ToArray().Join(",") : "");
			PlayerSettings.stripUnusedMeshComponents = stripUnusedMeshComponents;
			PlayerSettings.use32BitDisplayBuffer = use32BitDisplayBuffer;
			PlayerSettings.usePlayerLog = usePlayerLog;
			BuildTarget buildTarget = getEnumFromCompatibleOne<UnityEditor.BuildTarget>(buildTargetCopy.ToString());
			PlayerSettings.SetPropertyInt("ScriptingBackend", (int)scriptingBackend, buildTarget);

			PlayerSettings.SetPropertyInt("Architecture",(int)iPhoneArchitecture,buildTarget);
			PlayerSettings.virtualRealitySupported = virtualRealitySupported;
		}

		private List<GraphicsDeviceType_Copy> getGraphicsListFromGraphicsAPIs(UnityEditor.BuildTarget buildTarget)
		{
			var graphicsList = new List<GraphicsDeviceType_Copy>();
			var apis = PlayerSettings.GetGraphicsAPIs(buildTarget);
			var type = typeof (GraphicsDeviceType_Copy);
			foreach (var graphicsDeviceType in apis)
			{
				if (Enum.IsDefined(type, graphicsDeviceType.ToString()))
				{
					var graphics = (GraphicsDeviceType_Copy) Enum.Parse(type, graphicsDeviceType.ToString());
						//(typeof(UnityEngine.Rendering.GraphicsDeviceType), graphicsDeviceType.ToString());
					graphicsList.Add(graphics);
				}
				else
				{
					throw new Exception("Unrecognized graphics device tyupe -- will need to update list:" + graphicsDeviceType);
				}
			}
			return graphicsList;
		}

		private void setGraphicsAPIFromCopiesList(UnityEditor.BuildTarget buildTarget)
		{
			if (graphicsAPIs == null)
			{
				Debug.LogWarning("No graphics apis specified -- likely a depricated build info list");
				return;
			}

			var graphicsList = new List<UnityEngine.Rendering.GraphicsDeviceType>();

			foreach (var graphicsDeviceType in graphicsAPIs)
			{
				var type = typeof (UnityEngine.Rendering.GraphicsDeviceType);
				var inputGraphicsType = graphicsDeviceType.ToString();
				if (Enum.IsDefined(type, inputGraphicsType))
				{
					var graphics = (UnityEngine.Rendering.GraphicsDeviceType) Enum.Parse(type, inputGraphicsType);
						//(typeof(UnityEngine.Rendering.GraphicsDeviceType), graphicsDeviceType.ToString());
					graphicsList.Add(graphics);
				}
				else
				{
					Debug.LogError("Invalid graphics device tyupe:" + graphicsDeviceType);
				}
			}
			PlayerSettings.SetGraphicsAPIs(buildTarget, graphicsList.ToArray());
			//return graphicsList;
		}

		#region ios

		//public iOSSdkVersion iosSdkVersion;
		//public ScriptCallOptimizationLevel scriptCallOptimizationLevel;
		//public iOSTargetDevice iosTargetDevice;
		//TODO: since these are option in playersettings, look to see if these shoud go there
		//public ScriptingImplementation scriptingBackend; //Mono2x or IL2CPP
		//public iPhoneArchitecture iPhoneArchitecture; //ARMv7 or Universal
		//public UnityEditorCopies.GraphicsDeviceType[] graphicsAPIs;
		//prerenderedIcon

		#endregion

		#region android

		//public bool useAPKExpansionFiles;

		#endregion
	}
}