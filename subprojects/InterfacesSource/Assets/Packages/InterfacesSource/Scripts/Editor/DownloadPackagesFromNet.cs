﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using interfaces;

public class DownloadPackagesFromNet : ScriptableObject
{
	[MenuItem("quilt/Update current Packages %w")]
	private static void DownloadLatestPackages()
	{
		var www = new WWW(packagesUrl);
		ContinuationManager.Add(() => www.isDone, () =>
		{
			if(!string.IsNullOrEmpty(www.error)) Debug.Log("WWW failed: " + www.error);
			Debug.Log("WWW result : " + www.text);
			downloadAllPackages(www.text,false);
		});
	}
	[MenuItem("quilt/extended/Download All Latest Packages")]
	private static void DownloadAllPackages()
	{
		var www = new WWW(packagesUrl);
		ContinuationManager.Add(() => www.isDone, () =>
		{
			if(!string.IsNullOrEmpty(www.error)) Debug.Log("WWW failed: " + www.error);
			Debug.Log("WWW result : " + www.text);
			downloadAllPackages(www.text,true);
		});
	}
	//[MenuItem("Assets/Test2222")]
	static void downloadAllPackages(string sourceText,bool downloadNewPackages)
	{
		//string sourceText = @"<a href=""Analytics.unitypackage""> Analytics.unitypackage</a></li>";
		//string regexStr = "href=\"(.)*.unitypackage\":";
		string regexStr = "href=\"(.*)(.unitypackage)\"";
		Regex regex = new Regex(regexStr);
		Match match = regex.Match(sourceText);
		int totalMatches = 0;
		while(match.Success)
		{
			int currentmatch = ++totalMatches;
			var packageName = match.Groups[1]+".unitypackage";
			string finalDestinationOfPackage = ConfigBasedBuild.packagesPath() + Path.DirectorySeparatorChar +
												WWW.UnEscapeURL(packageName);
			if(!downloadNewPackages && !File.Exists(finalDestinationOfPackage))
			{
				match = match.NextMatch();
				continue;
			}
			Debug.Log(packageName);
			var www = new WWW(packagesUrl + packageName);
			ContinuationManager.Add(() => www.isDone, () =>
			{
				if(!string.IsNullOrEmpty(www.error)) Debug.Log("WWW failed: " + www.error);
				Debug.Log("Successfully downloaded:" + packageName + " currentmatch / totalmatches: " + currentmatch + " / " + totalMatches);
				File.WriteAllBytes(finalDestinationOfPackage, www.bytes);
			});
			match = match.NextMatch();
		}
		Debug.Log("download loop started");
	}
	private static string packagesUrl = "techiealex.com/packages/unity5/";

 
 public static class ContinuationManager
 {
     private class Job
     {
         public Job(Func<bool> completed, Action continueWith)
         {
             Completed = completed;
             ContinueWith = continueWith;
         }
         public Func<bool> Completed { get; private set; }
         public Action ContinueWith { get; private set; }
     }
 
     private static readonly List<Job> jobs = new List<Job>();
 
     public static void Add(Func<bool> completed, Action continueWith)
     {
         if (!jobs.Any()) EditorApplication.update += Update;
         jobs.Add(new Job(completed, continueWith));
     }
 
     private static void Update()
     {
         for (int i = 0; i >= 0; --i)
         {
             var jobIt = jobs[i];
             if (jobIt.Completed())
             {
                 jobIt.ContinueWith();
                 jobs.RemoveAt(i);
             }
         }
         if (!jobs.Any()) EditorApplication.update -= Update;
     }
 }
}