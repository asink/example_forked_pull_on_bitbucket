﻿using interfaces;
using UnityEngine;
using UnityEditor;

public static class BuildConfigurationExtensions
{
	public static void SetStaticInstanceFromThis(this BuildConfiguration config)
	{
		var serializedBuildSettingsEditor =
				config.editorUserBuildSettings.RemapObjectThroughSerialization<SerializedEditorUserBuildSettingsEditor>();
		var editorPlayerSettings = config.buildConfig.RemapObjectThroughSerialization<SerializedPlayerSettingsEditor>();

		serializedBuildSettingsEditor.SetStaticInstanceFromThis();
		editorPlayerSettings.SetStaticInstanceFromThis(config.editorUserBuildSettings.currentBuildTarget, config.editorUserBuildSettings.currentBuildTargetGroup);
		//editorUserBuildSettings.SetStaticInstanceFromThis();
		//playerSettings.SetStaticInstanceFromThis(config.editorUserBuildSettings.currentBuildTarget, config.editorUserBuildSettings.currentBuildTargetGroup);
	}

	public static void GetFromStaticInstance(this BuildConfiguration config)
	{
		var serializedBuildSettingsEditor =
				config.editorUserBuildSettings.RemapObjectThroughSerialization<SerializedEditorUserBuildSettingsEditor>();
		var editorPlayerSettings = config.buildConfig.RemapObjectThroughSerialization<SerializedPlayerSettingsEditor>();

		serializedBuildSettingsEditor.GetFromStaticInstance();
		config.editorUserBuildSettings = serializedBuildSettingsEditor;
		editorPlayerSettings.GetFromStaticInstance(config.editorUserBuildSettings.currentBuildTarget, config.editorUserBuildSettings.currentBuildTargetGroup);
		config.buildConfig = editorPlayerSettings;
	}
}