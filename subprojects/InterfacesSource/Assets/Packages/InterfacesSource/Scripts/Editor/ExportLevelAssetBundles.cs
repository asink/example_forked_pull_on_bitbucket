﻿// C# Example
// Builds an asset bundle from the selected objects in the project view.
// Once compiled go to "Menu" -> "Assets" and select one of the choices
// to build the Asset Bundle

using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace interfaces
{
	//orginal export from selection scripts http://docs.unity3d.com/ScriptReference/BuildPipeline.BuildAssetBundle.html
	public class ExportLevelAssetBundles
	{
		public static string CombinePaths(string first, params string[] others)
		{
			// Put error checking in here :)
			var path = first;
			foreach (var section in others)
			{
				path = Path.Combine(path, section);
			}
			return path;
		}
		//[MenuItem("Asink/test")]
		public static void isALevelProject()
		{
			Debug.Log(Application.dataPath.ToLower().Contains("_level"));
		}
		[MenuItem("quilt/level assetbundle/export to all default level targets")]
		public static void ExportAllCurrentTargets()
		{
			ExportMobileTargets();
			ExportDesktopTargets();
		}
		[MenuItem("quilt/level assetbundle/export to mobile targets")]
		public static void ExportMobileTargets()
		{
			ExportPlatformResource(BuildTarget.Android);
			ExportPlatformResource(BuildTarget.iOS);
		}
		[MenuItem("quilt/level assetbundle/export to desktop targets")]
		public static void ExportDesktopTargets()
		{
			ExportPlatformResource(BuildTarget.StandaloneWindows);
			ExportPlatformResource(BuildTarget.StandaloneWindows64);
			ExportPlatformResource(BuildTarget.StandaloneOSXIntel64);
		}
		[MenuItem("quilt/level assetbundle/Build android asset bundle from all loaded scenes")]
		public static void ExportAndroidResource()
		{
			ExportPlatformResource(BuildTarget.Android);
		}

		[MenuItem("quilt/level assetbundle/Build ios asset bundle from all loaded scenes")]
		public static void ExportResource()
		{
			ExportPlatformResource(BuildTarget.iOS);
		}

		[MenuItem("quilt/level assetbundle/Build win32 asset bundle from all loaded scenes")]
		public static void Exportwin32Resource()
		{
			ExportPlatformResource(BuildTarget.StandaloneWindows);
		}

		[MenuItem("quilt/level assetbundle/Build win64 asset bundle from all loaded scenes")]
		public static void Exportwin64Resource()
		{
			ExportPlatformResource(BuildTarget.StandaloneWindows64);
		}

		[MenuItem("quilt/level assetbundle/Build mac64 asset bundle from all loaded scenes")]
		public static void ExportmacResource()
		{
			ExportPlatformResource(BuildTarget.StandaloneOSXIntel64);
		}

		public static void ExportPlatformResource(BuildTarget targetPlatform,
			MobileTextureSubtarget androidsubTarget = MobileTextureSubtarget.ETC2)
		{
			//Path.GetFullPath(CombinePaths(Application.dataPath, "..", "testExported" + targetPlatform + "Level.unity3d"));

			//BuildOptions.BuildAdditionalStreamedScenes
			var paths = getScenePaths();
			if (paths.Count != 1)
			{
				EditorUtility.DisplayDialog("error",
					"number of exported scenes is not exactly 1, it is:" + paths.Count + " exported:" + paths.JsonSerialize(),
					"I'll fix it")
					;
				return;
			}

			var scenePath = paths[0];
			var sceneName = Path.GetFileNameWithoutExtension(scenePath);

			var fileName = "level_" + sceneName + "_" + targetPlatform.ToString().ToLower();
			//default2

			//BuildPipeline.BuildStreamedSceneAssetBundle(paths.ToArray(), path, targetPlatform);
			//AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);

			//bundle.Load("ExportedLevelManifest", typeof (ExportedLevelManfest));
			//bundle.Load("ExportedLevelManifest", typeof (ExportedLevelManfest));
			//unity5 builder
			//http://forum.unity3d.com/threads/dont-make-buildpipeline-buildassetbundle-obsolete.313995/
			//export package for consumption for local builds, default levels
			string levelsDirectory = ConfigBasedBuild.levelsPath();
			if(!Directory.Exists(levelsDirectory))
			{
				Directory.CreateDirectory(levelsDirectory);
			}
			var packageName = fileName;
			var packagePath =
				Path.GetFullPath(CombinePaths(ConfigBasedBuild.levelsPath(), packageName + ".unitypackage"));
			var sharedScriptsPath = Path.GetFullPath(CombinePaths(ConfigBasedBuild.levelsPath(), "level_" + sceneName + "_shared_scripts" + ".unitypackage"));
			Debug.Log("all paths exported:" + string.Join(",", paths.ToArray()) + " target path:" + packagePath);

			if(!Directory.Exists("Assets/StreamingAssets"))
				Directory.CreateDirectory("Assets/StreamingAssets");
			var targetPath = "Assets/StreamingAssets";
			//Directory.CreateDirectory(targetPath);
			BuildPipeline.BuildAssetBundles(targetPath, new[]
			{
				new AssetBundleBuild
				{
					assetBundleName = packageName, //asink: .unity3d was old thing, not sure about this
					assetNames = paths.ToArray()
				}
			},BuildAssetBundleOptions.None,targetPlatform);
			//Debug.Log("path output:" + pGetFullPathackagePath + " input:" + path);
			//BuildPipeline.BuildAssetBundle(AssetDatabase.LoadAssetAtPath(outputPath, typeof (Object)), new Object[] {},
			//	Path.GetFullPath(CombinePaths(ConfigBasedBuild.subProjectsPath(), "Level_" + LevelSuffix + ".unitypackage")));
			//EditorUserBuildSettings.SwitchActiveBuildTarget(targetPlatform);
			//EditorUserBuildSettings.androidBuildSubtarget = androidsubTarget;

			AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);

			string[] filenames = new[] {"Assets/StreamingAssets/" + fileName, "Assets/StreamingAssets/" + fileName + ".manifest"};
			Debug.Log("Looking for:"+filenames.JsonSerialize());
			AssetDatabase.ExportPackage(filenames, packagePath, ExportPackageOptions.Recurse);
			//export the shared scripts
			AssetDatabase.ExportPackage("Assets/SharedScripts", sharedScriptsPath, ExportPackageOptions.Recurse);
		}

		//input ie "Scenes/MyScene.unity3d" will be fals -- as MyScene is not valid.  
		private static List<string> getScenePaths()
		{
			var paths = new List<string>();
			foreach (var scene in EditorBuildSettings.scenes)
			{
				string error = null;
				if (scene.enabled == false)
					continue;
				Action reportError = () =>
				{
					Debug.LogError(error);
					if (EditorUtility.DisplayDialog("error",
						error,
						"I'll fix it"))
					{
						throw new Exception("Ok, fix it and then re-export :)");
					}
				};
				var sceneName = Path.GetFileNameWithoutExtension(scene.path);
				if (!sceneFileIsValid(scene.path, out error))
				{
					reportError();
				}

				var sceneGO = GameObject.Find(sceneName);

				if (sceneGO == null)
				{
					error = "No gameobject found with scene name in heirarchy :" + sceneName + " test1";
					reportError();
				}

				if (sceneGO.transform.parent != null)
				{
					error = "either multiple gameobjects with scene name or parent of scene go is not at the root level :" + sceneName;
					reportError();
				}
				foreach (var go in Object.FindObjectsOfType<GameObject>())
				{
					if (go == sceneGO)
						continue;
					if (go.transform.parent == null)
					{
						error = "active gameobject at root -- there should only be a single gameobject with name:" + sceneName +
						        " at root level, and the go named:" + go.name + " is active in the scene";
						reportError();
					}
				}

				paths.Add(scene.path);
			}

			return paths;
		}

		private static bool sceneFileIsValid(string scenePath, out string error)
		{
			error = null;
			//var sceneFileName = Path.GetFileName(scenePath); //FileUtil needed to go to native file directory seperators?

			var sceneNoExtension = Path.GetFileNameWithoutExtension(scenePath);

			//NOTE: replacing assets in scenepath since we know the fs path directory seperator is "/" and not sure in Application.atapath
			//the Assets folder is both
			var filesystemPath = Path.GetFullPath(Path.Combine(Application.dataPath, scenePath.Replace("Assets/", "")));
			var fsFileName = Path.GetFileNameWithoutExtension(filesystemPath);

			if (sceneNoExtension.ToLower() != sceneNoExtension)
			{
				error =
					"sceneFileName is not all lowercase -- you may need to add/remove it again from the build settings sceneNoExtension name:" +
					sceneNoExtension;
				return false;
			}
			if (sceneNoExtension != fsFileName)
			{
				error = "fs file name is not the same as scene file name:" + sceneNoExtension + " versus fs file name:" + fsFileName;
				return false;
			}
			if (!File.Exists(filesystemPath))
			{
				error =
					"file on actual filesystem doesn't match that in the edior settings, check that the name in the asset path is lowercase, and add it back to the editor build settings -- expected filesystem path:" +
					filesystemPath + " and raw scenepath is :" + scenePath;
				return false;
			}
			if (!EditorApplication.OpenScene(scenePath))
			{
				error = " could not open scene by name:" + scenePath;
				return false;
			}
			return true;
		}
	}
}