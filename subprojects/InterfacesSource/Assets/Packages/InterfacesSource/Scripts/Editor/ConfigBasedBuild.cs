﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace interfaces
{
	public class ConfigBasedBuild
	{
		//TODO: move this to a test
		//public static string testConfigString =
		//	@"{""app_output_name"":""myTest"",""buildOptions"":""CompressTextures"",""editorUserBuildSettings"":{""allowDebugging"":false,""currentBuildTarget"":""StandaloneWindows"",""currentBuildTargetGroup"":""Standalone"",""development"":false,""explicitNullChecks"":false,""subtarget"":""Generic"",""symlinkLibraries"":false},""buildConfig"":{""bundleIdentifier"":""com.Company.ProductName"",""bundleVersion"":""1.0"",""companyName"":""DefaultCompany"",""defaultInterfaceOrientation"":""AutoRotation"",""defaultIsFullScreen"":true,""defaultScreenHeight"":768,""defaultScreenWidth"":1024,""displayResolutionDialog"":""Disabled"",""productName"":""Interfaces"",""renderingPath"":""Forward"",""scriptingDefineSymbols"":[""""],""stripUnusedMeshComponents"":false,""supportedAspectRatios"":[""AspectOthers"",""Aspect4by3"",""Aspect5by4"",""Aspect16by10"",""Aspect16by9""],""use32BitDisplayBuffer"":true,""useDirect3D11"":true,""usePlayerLog"":true},""InterfaceMapping"":{},""packagesToAdd"":[""foo"",""bar"",""baz""]}";
		
		[MenuItem("quilt/extended/print current build settings")]
		private static void PrintBuildSettings()
		{
			//should this be pulled from the first scene? or no?
			//what's the relationship to the original scene
			var interfaceMap = new InterfaceMap
			{
				interfaceMap = new Dictionary<string, string> {{"IVR", "DiveVR"}},
				interfaceToMultipleMaps = new Dictionary<string, List<string>>(),
				namedIntTypes = new Dictionary<string, int>(),
				namedStringTypes = new Dictionary<string, string>()
			};

			var config = new BuildConfiguration
			{
				app_output_name = "myTest",
				//buildOptions = Application.build
				editorUserBuildSettings = new SerializedEditorUserBuildSettings(),
				buildConfig = new SerializedPlayerSettings(),
				InterfaceMapping = interfaceMap,
				packagesToAdd = new List<string> {"foo", "bar", "baz"}
			};

			config.GetFromStaticInstance();

			Debug.Log("Sample config :" + config.JsonSerialize());
		}
		public static bool thisProjectIsASubproject()
		{
			string appPath = Application.dataPath;
			appPath = Path.GetFullPath(appPath);
			return appPath.Contains(subProjectsPath());
			//Application.dataPath.Replace("\\", "/").Contains(ConfigBasedBuild.subProjectsPath());
		}

		//[MenuItem("asink/sub test")]
		public static void testSub()
		{
			Debug.Log("Is a subproejct:" + thisProjectIsASubproject());
		}
		private static void SetDefaultMappingsFile(InterfaceMap map)
		{
			var pathOfDefaultMappings = AssetDatabase.GetAssetPath(Resources.Load<TextAsset>("DefaultMappings"));
			//Debug.Log("path of default mappings:" + pathOfDefaultMappings + " text:" + File.ReadAllText(pathOfDefaultMappings));
			File.WriteAllText(pathOfDefaultMappings, map.JsonSerialize());
			AssetDatabase.Refresh(); //make sure that unity knows the file changed... may not need to do this
			AssetDatabase.SaveAssets();
		}

		[MenuItem("quilt/extended/CI/Load config file for current platform")]
		public static BuildConfiguration LoadConfigFileForCurrentPlatform()
		{
			return LoadConfigFile(CurrentPlatformConfigFile());
		}

		[MenuItem("quilt/extended/CI/Load config android dive ")]
		public static BuildConfiguration LoadAndroidDiveConfig()
		{
			return LoadConfigFile("Android_ETC2PackageNameInfoDive.txt");
		}

		public static string CurrentPlatformConfigFile()
		{
			return currentHumanReadiblePlatformName() + "PackageNameInfo.txt";
		}

		public static string PlatformConfigFile(BuildTarget target,
			MobileTextureSubtarget subtarget = MobileTextureSubtarget.Generic)
		{
			return HumanReadiblePlatformName(target, subtarget) + "PackageNameInfo.txt";
		}

		private static string currentHumanReadiblePlatformName()
		{
			return HumanReadiblePlatformName(EditorUserBuildSettings.activeBuildTarget,
				EditorUserBuildSettings.androidBuildSubtarget);
		}

		private static string HumanReadiblePlatformName(BuildTarget target,
			MobileTextureSubtarget subtarget = MobileTextureSubtarget.Generic)
		{
			return target == BuildTarget.Android ? target + "_" + subtarget : target.ToString();
		}
		[MenuItem("quilt/extended/debug/print levelsPath directory")]
		public static string configsPath()
		{
			return findDirectoryInParentHeirarchy("configs");
		}
		static string findDirectoryInParentHeirarchy(string folderName, string basePath = null)
		{
			if(basePath == null)
				basePath = Application.dataPath;
			//recurse backwards through the directories until you hit the subprojects path
			var startingPath = Path.GetFullPath(basePath);
			var path = startingPath;
			while(!string.IsNullOrEmpty(path) && path.Length > 5)
			{
				var speculativeSubprojectsDir = Path.Combine(path, folderName);
				//Debug.Log("Checking:"+speculativeSubprojectsDir);
				if(Directory.Exists(speculativeSubprojectsDir))
					return speculativeSubprojectsDir;

				path = Directory.GetParent(path).FullName;
			}
			throw new Exception("no " + folderName + " path found");
		}
		public static BuildConfiguration LoadConfigFile(string fileName)
		{
			var filePath = configsPath() + Path.DirectorySeparatorChar + fileName;
			var text = File.ReadAllText(filePath);
			var config = text.JsonDeserialize<BuildConfiguration>();
			config.SetStaticInstanceFromThis();

			SetDefaultMappingsFile(config.InterfaceMapping);
			EditorApplication.SaveAssets();

			return config;
		}
		[MenuItem("quilt/extended/debug/print subProjectsPath directory")]
		public static string subProjectsPath()
		{
			return findDirectoryInParentHeirarchy("subprojects");
		}

		[MenuItem("quilt/extended/debug/print packagesPath directory")]
		public static string packagesPath()
		{
			//ie C:\dev\unity5packages\subprojects\InterfacesSource vs C:/dev/unity5packages/subprojects/InterfacesSource/Assets
			//Debug.Log(findDirectoryInParentHeirarchy("packages", Directory.GetParent(Application.dataPath).FullName));
			return findDirectoryInParentHeirarchy("packages", Directory.GetParent(Application.dataPath).FullName);
		}
		[MenuItem("quilt/extended/debug/print levelsPath directory")]
		public static string levelsPath()
		{
			return Path.GetFullPath(Path.Combine(packagesPath(), "levels"));
		}
		public static void ImportPackage(string packageName)
		{
			if(thisProjectIsASubproject())
				throw new Exception("Importing packages from a subpcakge -- almost certainly a mistake, do this manually instead");
			if(packageName.Contains("levels/"))
				packageName = packageName.Replace("levels/", "levels" + Path.DirectorySeparatorChar);
			else
			{
				if(packageName.Contains("level_"))
					packageName = "levels" + Path.DirectorySeparatorChar + packageName;
			}
			var packagePath = packagesPath() + Path.DirectorySeparatorChar + packageName + ".unitypackage";
			Debug.Log("Package path is:" + packagePath);
			AssetDatabase.ImportPackage(packagePath, false);
		}
		[MenuItem("quilt/extended/testfoo/testing menu item2")]
		public static void TestThing()
		{
		}
	}
}