﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace interfaces
{
	public class ExportSelectionAsPackage
	{
		private static readonly UnityLogger logger = new UnityLogger();

		public static bool isMainProject()
		{
			return (Application.dataPath.ToLower().Contains("mainproject"));
		}
		private static bool isALevelProject()
		{
			return (Application.dataPath.ToLower().Contains("_level"));
		}
		public static string absolutePathFromProjectPath(string projectPath)
		{
			return Path.GetFullPath(string.Join(Path.DirectorySeparatorChar + "", new[] { Application.dataPath, projectPath }));
		}

		static void GetDLLToImportBySettingDirtyAndImportingExplicitly()
		{
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
			//
			//just set everything dirty, since the above force import doesn't seem to detect changed files, especilly if they were updated outside of unity and then project opened
			var files = Directory.GetFiles(Application.dataPath, "*");
			AssetDatabase.StartAssetEditing();
			foreach(var file in files)
			{
				string cleanPath = FileUtil.GetProjectRelativePath(Path.GetFullPath(file).Replace("\\", "/"));
				if(cleanPath.Contains(".meta"))
					continue;

				if(cleanPath.Contains(".dll"))  //maybe this is the magic that gets dlls to acutally re-hook into the editor?
				{
					EditorUtility.SetDirty(AssetDatabase.LoadAssetAtPath(cleanPath, typeof(UnityEngine.Object)));
					AssetDatabase.ImportAsset(cleanPath, ImportAssetOptions.ForceSynchronousImport | ImportAssetOptions.ForceUpdate);
				}
			}
			AssetDatabase.StopAssetEditing();
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
		}
		[MenuItem("Assets/Export as quilt Package %e")]
		[MenuItem("quilt/Export as quilt Package %e")]
		public static void ExportAsPackage()
		{
			//AssetDatabase.Refresh();
			if(isMainProject())
			{
				Debug.LogError("Tried to export package as main project -- this has overwritten analytics many times over, so disablign this");
				return;
			}
			if (isALevelProject())
			{
				Debug.Log("exporting level to all current targets");
				ExportLevelAssetBundles.ExportAllCurrentTargets();
				return;
			}
			GetDLLToImportBySettingDirtyAndImportingExplicitly();

			var packagePath = absolutePathFromProjectPath("Packages");
			var isPackageExport = Directory.Exists(packagePath);
			var pathToExport = absolutePathFromProjectPath("Interfaces");
			if(isPackageExport)
				pathToExport = Directory.GetDirectories(packagePath)[0];

			var pathElements = pathToExport.Split(new[] { Path.DirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);
			var packageName = pathElements[pathElements.Length - 1];
			var destinationOfpackage = ConfigBasedBuild.packagesPath();

			var completeDestinationPath = destinationOfpackage + Path.DirectorySeparatorChar + packageName + ".unitypackage";
			logger.LogFormat("DestinationOfPackage:{0} packageName:{1} path to export{2} output unitypackage:{3}",
				destinationOfpackage, packageName, pathToExport, completeDestinationPath);
			var pathsToExport = new List<string> { isPackageExport ? "Assets/Packages" : "Assets/Interfaces" };

			var pluginsDir = absolutePathFromProjectPath("Plugins");
			if(Directory.Exists(pluginsDir))
				pathsToExport.Add("Assets/Plugins");
			//TODO: create the manifest in a .meta file
			//just check to see if the paths have packages, interfaces, or plugins in the anem
			AssetDatabase.ExportPackage(pathsToExport.ToArray(), completeDestinationPath, ExportPackageOptions.Recurse);
		}

		
	}
}