﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using strange.extensions.command.impl;
using UnityEngine;

namespace interfaces
{
	//sends all *txt files in your temporary cache path off
	public class SendAllLogFilesCommand : Command
	{
		[Inject]
		public BuildConfigSO buildConfig { get; set; }

		[Inject]
		public IRoutineRunner runner { get; set; }

		[Inject]
		public IWebService webSerivice { get; set; }

		[Inject]
		public ILogger logger { get; set; }

		[Inject(NamedInjectionsCore.LOGFILE_ENDPOINT)]
		public string logfileEndpoint { get; set; }

		public override void Execute()
		{
			SendCachedFiles();
		}

		private void SendCachedFiles()
		{
			if (runner == null)
				throw new Exception("RUNNER IS NULL ___ WTF	");
			var logFiles = Directory.GetFiles(Application.temporaryCachePath, "*txt");
			runner.StartCoroutine(SendLogFiles(logFiles));
		}

		private IEnumerator SendLogFiles(string[] paths)
		{
			foreach (var path in paths)
			{
				//Debug.Log("Sending file:" + path);

				logger.Log("Sending file:" + path);
				byte[] fileBytes = null;
				try
				{
					fileBytes = File.ReadAllBytes(path);
					if (fileBytes.Length == 0)
					{
						logger.LogWarning("zero size file, deleting:"+path);
						File.Delete(path);
						continue;
					}

				}
				catch // (Exception e)
				{
					//Debug.Log("could not read(to send to server) file:" + path + " because of exception :" + e);
					continue;
				}

				var param = new WWWFormParams
				{
					BinaryPostParams = new Dictionary<string, byte[]> {{"theFile", fileBytes}},
					TypeOfRequest = WWWFormParams.RequestType.BINARY_POST,
					GetParams =
						new Dictionary<string, string>
						{
							{"User", SystemInfo.deviceUniqueIdentifier},
							{"Platform", Application.platform.ToString()},
							{"ProductName", buildConfig.ProductName},
							{"BuildNumber", buildConfig.BuildNumber.ToString()},
							{"BuildType", buildConfig.BuildType.ToString()}
						}
				};
				var results = new WebServiceStatus();
				var url = logfileEndpoint;
				yield return runner.StartCoroutine(webSerivice.SendRequest(url, param, results));
				
				if (results.Status == ReturnStatus.SUCCESS)
				{
					File.Delete(path);
				}
				else
				{
					logger.LogWarning("error file send STATUS:" + results.JsonSerialize());
				}
					
				yield return new WaitForSeconds(1f);
			}
		}
	}
}