﻿using System;
using System.Collections;
using UnityEngine;

namespace interfaces
{
	//This is the worst name I can think of, but this needed to move out of the helloworldcontextview
	//should 
	[Serializable]
	public class InterfaceMappingUtilities
	{
		public InterfaceMap InterfaceMap;

		public InterfaceMap GetDefaultMap()
		{
			var mapJson = Resources.Load<TextAsset>("DefaultMappings");
			return mapJson.text.JsonDeserialize<InterfaceMap>();
		}

		public IEnumerator LoadInterfaceMapFromNet(IRoutineRunner runner)
		{
			InterfaceMap = GetDefaultMap();

			var requestor = new DownloadInterfaceMapFromMyHost();
			yield return runner.StartCoroutine(requestor.getCollabEditDocument());
			if (requestor.output != null && requestor.output.map != null)
				InterfaceMap = requestor.output.map;
		}

		public void SetFrameRate()
		{
			if (Application.isEditor || (!Application.isMobilePlatform))
				Application.targetFrameRate = 75;
			else
				Application.targetFrameRate = 60;
		}
	}
}