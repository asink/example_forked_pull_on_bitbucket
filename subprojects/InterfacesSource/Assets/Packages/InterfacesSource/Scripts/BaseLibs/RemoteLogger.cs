﻿using UnityEngine;

namespace interfaces
{
	[Implements]
	public class RemoteLogger : ILogger
	{
		private FileWriter _writer;

		[Inject]
		public BuildConfigSO buildConfig { get; set; }

		[Inject]
		public IRoutineRunner runner { get; set; }

		[Inject]
		public IWebService webSerivice { get; set; }

		public void Log(object toLog)
		{
			Log(toLog.ToString());
		}

		public void LogFormat(string format, params object[] parameters)
		{
			Log(string.Format(format, parameters));
		}

		public void LogWarning(object toLog)
		{
			Log("WARNING:" + toLog);
		}

		public void LogWarningFormat(string format, params object[] parameters)
		{
			Log(string.Format("WARNING:" + format, parameters));
		}

		public void LogError(object toLog)
		{
			Log("ERROR:" + toLog);
		}

		public void LogErrorFormat(string format, params object[] parameters)
		{
			Log(string.Format("ERROR:" + format, parameters));
		}

		[PostConstruct]
		public void PostConstruct()
		{
#if !UNITY_5
			Application.RegisterLogCallback(ApplicationOnLogMessageReceived);
#else
			Application.logMessageReceived += ApplicationOnLogMessageReceived;
#endif
		}

		private void ApplicationOnLogMessageReceived(string condition, string stackTrace, LogType type)
		{
			LogFormat("UNITYLOG TYPE:{0} condition:{1} stack:{2}", type, condition, stackTrace);
		}

		private void Log(string toLog)
		{
			if (_writer == null)
			{
				_writer = new FileWriter("LogFile");
			}
			_writer.Write(toLog);
		}
	}
}