using System.Collections.Generic;

public class CountlyEvent
{
    public CountlyEvent()
    {
        Key = "";
        UsingSum = false;
        UsingSegmentation = false;
    }

    /// <summary>
    /// String describing the event that has occured. (Mandatory)
    /// </summary>
    public string Key { get; set; }
    /// <summary>
    /// The number of times this event has occured. (Mandatory)
    /// </summary>
    public int Count { get; set; }

    /// <summary>
    /// Flags if Sum will be used in the event call. Automatically set
    /// when Sum is modified.
    /// </summary>
    public bool UsingSum { get; private set; }

    private double? _sum;
    /// <summary>
    /// Value used in the summation of similar event. For example the price
    /// of an in app purchase, so total revenue can be monitored. (Optional)
    /// </summary>
    public double? Sum
    {
        get { return _sum; } 
        set
        {
            UsingSum = true; 
            _sum = value;
        }
    }

    /// <summary>
    /// Flags if Segmentation will be used in the event call. Automatically
    /// set when Segmentation is modified.
    /// </summary>
    public bool UsingSegmentation { get; private set; }

    private Dictionary<string, string> _segmentation;

    /// <summary>
    /// Used to define characteristics of the event which can be filtered by. (Optional)
    /// </summary>
    public Dictionary<string, string> Segmentation
    {
        get { return _segmentation; } 
        set
        {
            UsingSegmentation = true;
            _segmentation = value;
        }
    }
}