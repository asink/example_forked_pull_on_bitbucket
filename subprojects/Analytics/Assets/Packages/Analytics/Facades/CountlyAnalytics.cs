﻿using System.Collections.Generic;
using System.Diagnostics;
using interfaces;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace implementations
{
	//[Implements(typeof(IAnalytics),InjectionBindingScope.SINGLE_CONTEXT,AnalyticsManager.AnalyticsType.PRIMARY)]
	public class CountlyAnalytics : IAnalytics
	{
		private readonly Dictionary<string, Stopwatch> _timedEvents = new Dictionary<string, Stopwatch>();

		[Inject]
		public BuildConfigSO buildConfig { get; set; }

		//public string AppKey = "fd635e67659c91b954fbb3dce72c9ed73a906c7c";
		private Countly _analytics;

		public void Init(List<string> initializationInfo)
		{
			_analytics = SingletonManager.instance.AddSingleton<Countly>();
			_analytics.ServerURL = "https://cloud.count.ly";
			_analytics.AppKey = initializationInfo[0];
			_analytics.AppVersion = buildConfig.BuildNumber.ToString();
			_analytics.IsDebugModeOn = false;
			_analytics.AutoStart = true; //makes start/end irrelevant, I think?
		}

		public void StartSession()
		{
		}

		public void EndSession()
		{
			foreach (var timedEvent in _timedEvents)
			{
				//NOTE: remaining timed events will likely be dropped, as the api fails on the last frame.  that can be corrected by saving unsent data on close
				Log(timedEvent.Key, true); //kill all timed events.. even if  in the api
			}
		}

		public void Log(string EventName, bool isTimed = false)
		{
			if(isTimed)
			{
				if(_timedEvents.ContainsKey(EventName))
				{
					Stopwatch sw = _timedEvents[EventName];
					Log(EventName + "Timed",
						new Dictionary<string, string> {{"Time", Mathf.RoundToInt((float) sw.Elapsed.TotalSeconds).ToString()}});
					_timedEvents.Remove(EventName);
				} else
				{
					var sw = new Stopwatch();
					sw.Start();
					_timedEvents.Add(EventName, sw);
				}
			}
			_analytics.PostEvent(new CountlyEvent {Key = EventName});
		}

		public void Log(string EventName, Dictionary<string, string> parameters)
		{
			_analytics.PostEvent(new CountlyEvent {Key = EventName, Segmentation = parameters});
		}

	}
}