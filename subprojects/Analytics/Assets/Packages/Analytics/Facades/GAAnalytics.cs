﻿using System;
using System.Collections.Generic;
using interfaces;

namespace implementations
{
	public class GoogleAnalytics : IAnalytics
	{
		private GoogleAnalyticsV3 _coreThing;
		[Inject]
		public BuildConfigSO buildConfig { get; set; }

		public void Init(List<string> initializationInfo)
		{
			SerializedGAAnalyticsInitParams initParams = initializationInfo[0].JsonDeserialize<SerializedGAAnalyticsInitParams>();
			//UnityEngine.Debug.Log("GoogleAnalytics init" + initParams.JsonSerialize());
			Init(initParams,buildConfig.ProductName);
			//_coreThing = _analytics.gameObject.add
		}

		public void StartSession()
		{
			_coreThing.StartSession();
			_coreThing.LogEvent(new EventHitBuilder().SetEventAction("StartSession").SetEventCategory("Session"));
		}

		public void EndSession()
		{
			_coreThing.LogEvent(new EventHitBuilder().SetEventAction("EndSession").SetEventCategory("Session"));
		}

		public void Log(string EventName, bool isTimed = false)
		{
			_coreThing.LogEvent(new EventHitBuilder().SetEventAction(EventName).SetEventCategory("CustomEvents"));
		}

		public void Log(string EventName, Dictionary<string, string> parameters)
		{
			_coreThing.LogEvent(new EventHitBuilder().SetEventAction(EventName).SetEventCategory("CustomEvents").Validate());
		}

		[Serializable]
		public class SerializedGAAnalyticsInitParams
		{
			public string androidTrackingCode;
			public string IOSTrackingCode;
			public string otherTrackingCode;
			public string productName;
			public string bundleIdentifier;
			public string bundleVersion;
			public int dispatchPeriod = 5;
			public float sampleFrequency = 100.0F;
			public GoogleAnalyticsV3.DebugMode logLevel = GoogleAnalyticsV3.DebugMode.WARNING;
			public bool anonymizeIP = false;
			public bool dryRun = false;
		}

		public void Init(SerializedGAAnalyticsInitParams init, string productName)
		{
			var gaRunner = SingletonManager.instance.gameObject.AddComponent<GoogleAnalyticsV3>();
			gaRunner.androidTrackingCode = init.androidTrackingCode;
			gaRunner.IOSTrackingCode = init.IOSTrackingCode;
			gaRunner.otherTrackingCode = init.otherTrackingCode;
			gaRunner.productName = init.productName;
			gaRunner.bundleIdentifier = init.bundleIdentifier;
			gaRunner.bundleVersion = init.bundleVersion;
			gaRunner.dispatchPeriod = init.dispatchPeriod;
			gaRunner.sampleFrequency = init.sampleFrequency;

			
			_coreThing = gaRunner;
		}
	}
}