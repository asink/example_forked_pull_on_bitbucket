﻿using UnityEngine;
using System;
using System.Collections;

namespace interfaces
{
	public interface IMiscNativeOptions
	{
		void Alert(string title, string message);
		void EmailComposer(string toAddress, string subject, string text);
		void ScheduleNotification(DateTime timeToSchedule, string title, string subtitle);
		void CancelAllNotifications();
		//bool launchedFromNotifications();
	}
}