﻿using System;
using interfaces;
using UnityEngine;

namespace implementations
{
#if UNITY_ANDROID
	[Implements(typeof(IMiscNativeOptions))]
	public class AndroidEtcetera : IMiscNativeOptions
	{
		public void Alert(string title, string message)
		{
			EtceteraAndroid.showAlert(title,message,"ok");
		}

		public void EmailComposer(string toAddress, string subject, string text)
		{
			EtceteraAndroid.showEmailComposer(toAddress,subject,text,false);
		}

		public void ScheduleNotification(DateTime timeToSchedule, string title, string subtitle)
		{
			EtceteraAndroid.scheduleNotification(Mathf.RoundToInt((float) (DateTime.Now - timeToSchedule).TotalSeconds), title,
				subtitle, title, "");
		}

		public void CancelAllNotifications()
		{
			EtceteraAndroid.cancelAllNotifications();
		}
	}
#endif
}

