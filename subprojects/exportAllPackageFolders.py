#!/usr/bin/env python
from os import listdir
from os.path import isfile, isdir, join
import os, sys
import time
from time import sleep
from subprocess import call
import traceback
import shutil
from shutil import copytree
import sys
'''

projectDir=sys.argv[2]
print projectDir
configFile=projectDir+"/subProjects/"+sys.argv[3]
'''
unity=sys.argv[1]
print unity
SubProjectsDir=os.path.dirname(os.path.realpath(__file__))

def callUnity(targetProjectPath,method):
    #executionString = '/Volumes/1tbhdd/Applications/Unity461f1/Unity.app/Contents/MacOS/Unity -projectPath /Volumes/1tbhdd/Users/FromOtherDrive/dev/ggj15/MainProject -quit -batchmode -importPackage /Volumes/1tbhdd/Users/FromOtherDrive/dev/ggj15/subProjects/Analytics.unitypackage'
    executionString = "\"%s\" -projectPath \"%s\" -quit -batchmode -nographics -executeMethod %s" % \
                      (unity, targetProjectPath, method )

    #call("echo 'hello world'",shell=True)
    print executionString
    call(executionString, shell=True)
    sleep(0.1)
    sys.stdout.flush()

levelsDir=os.path.join(SubProjectsDir,"levels")
foldersToScan = os.listdir(SubProjectsDir)
try:
    foldersToScan = foldersToScan + os.listdir(levelsDir)
except:
    print "no levels here"

targetFolders = []

for file in foldersToScan: #os.walk("."):
    filePath= os.path.join(SubProjectsDir,file)
    if("_level".lower() in file.lower()):
        filePath = os.path.join(levelsDir,file)
    if(os.path.isdir(filePath) is False):
        continue
    if(file.lower() == "InterfacesSource".lower()):
        continue
    print filePath
    targetFolders.append(filePath)

for folder in targetFolders:
    if("_level".lower() in folder.lower()):
        callUnity(folder,"interfaces.ExportLevelAssetBundles.ExportAllCurrentTargets")
    else:
        callUnity(folder,"interfaces.ExportSelectionAsPackage.ExportAsPackage")



