﻿using interfaces;
using UnityEngine;

namespace implementations
{
	public class SoundTestContext : HelloWorldContext
	{
		public SoundTestContext(MonoBehaviour contextView, bool autostart)
			: base(contextView, autostart)
		{
		}

		public SoundTestContext(MonoBehaviour contextView, bool autostart, InterfaceMappingUtilities map)
			: base(contextView, autostart, map)
		{
		}

		protected override void mapBindings()
		{
			base.mapBindings();
			//injectionBinder.Bind<IWebService>().To<UnityWWWWebSerivce>();
			commandBinder.Bind<StartSignal>().To<LogClientInfoCommand>();
			//.To<LoadMainLevelCommand>();
		}
	}
}
