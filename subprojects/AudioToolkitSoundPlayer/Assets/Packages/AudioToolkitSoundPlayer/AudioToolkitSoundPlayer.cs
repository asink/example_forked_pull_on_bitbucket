﻿using System;
using System.Collections;
using interfaces;
using UnityEngine;
using Object = UnityEngine.Object;

namespace implementations
{
	[Implements(typeof(ISoundPlayer))]
	public class AudioToolkitSoundPlayer : ISoundPlayer
	{
		private AudioController _controller;

		[Inject]
		public IRoutineRunner runner { get; set; }

		[Inject]
		public IAnalytics analytics { get; set; }

		[Inject]
		public ILogger logger { get; set; }
		[Inject]
		public LoadAssetBundleService bundleLoader { get; set; }

		public float PlaySound(string soundName)
		{
			var audioInfo = AudioController.Play(soundName);
			if(audioInfo == null)
			{
				logger.LogWarning("could not play sound:"+soundName);
				return 0f;
			}
			return audioInfo.clipLength;
		}

		public float PlaySound(string soundName, Vector3 position)
		{
			return AudioController.Play(soundName, position).clipLength;
		}

		public void StopSound(string soundName)
		{
			AudioController.Stop(soundName);
		}

		public void StopGroup(SoundGroup group)
		{
			AudioController.StopCategory(group.ToString());
		}

		public void StopGroup(string group)
		{
			AudioController.StopCategory(group);
		}

		public void SetGlobalVolume(float weight)
		{
			AudioController.SetGlobalVolume(weight);
		}

		public float GetGlobalVolume()
		{
			return AudioController.GetGlobalVolume();
		}

		public void SetGroupVolume(SoundGroup group, float weight)
		{
			AudioController.SetCategoryVolume(group.ToString(), weight);
		}

		public float GetGroupVolume(SoundGroup group)
		{
			return AudioController.GetCategoryVolume(group.ToString());
		}

		public IEnumerator Init()
		{
			if(_controller != null)
				yield break;

			//analytics.Log("loadAudioTime", true);
			yield return runner.StartCoroutine(bundleLoader.LoadAssetBundle("AudioController", BUNDLE_TYPE.AUDIO));


			_controller = Object.FindObjectOfType<AudioController>();

		}

		public void Dispose()
		{
			if(runner != null)
			{
				bundleLoader.UnloadAssetBundle("AudioController");
			}
			
		}
		[PostConstruct]
		public void PostConstruct()
		{
			//Debug.Log("Audio init");
			//runner.StartCoroutine(loadAudioStreamedAsset());
		}
	}
}