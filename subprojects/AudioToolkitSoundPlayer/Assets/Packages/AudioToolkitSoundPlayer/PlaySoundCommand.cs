﻿using System.Collections;
using interfaces;
using strange.extensions.command.impl;

namespace implementations
{
	public class PlaySoundCommand : Command
	{
		[Inject]
		public ISoundPlayer player { get; set; }
		[Inject]
		public IRoutineRunner runner { get; set; }

		public override void Execute()
		{
			Retain();
			runner.StartCoroutine(testthing());
		}

		IEnumerator testthing()
		{
			//yield return new WaitForSeconds(1f);
			yield return runner.StartCoroutine(player.Init());
			//yield return new WaitForSeconds(1f);
			player.PlaySound("VO_1");
			Release();
		}
	}

}