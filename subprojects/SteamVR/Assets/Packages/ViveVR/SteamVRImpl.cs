﻿using UnityEngine;
using System.Collections.Generic;
using interfaces;
using Valve.VR;

namespace implementations
{
	[Implements(typeof(IVR))]
	public class SteamVRImpl : IVR
	{
		private readonly CameraLoader _loader;
		private readonly List<CameraCommands> _supportedCommands;
		//List<SteamVR_Utils.RigidTransform> eyes = new List<SteamVR_Utils.RigidTransform>();
		public SteamVRImpl()
		{
			_loader = new CameraLoader("SteamVR");
			
			_supportedCommands = new List<CameraCommands>
			{
			};
			
		}
		public GameObject CreateCamera()
		{
			return _loader.CreateCamera();
		}

		public CameraSettingsSO GetCameraSettingsSO()
		{
			return _loader.CameraSettings;
		}

		public void CameraCommand(CameraCommands command)
		{
		}

		public void SetCustomCameraSettings(string settingString)
		{
		}

		public List<CameraCommands> GetSupportedCommands()
		{
			return _supportedCommands;
		}

		public Vector3 Position(VRComponent component)
		{
			if (VRComponent.LEFT_EYE == component)
			{
				return SteamVR.instance.eyes[0].pos;
			}
			return SteamVR.instance.eyes[1].pos;
		}

		public Quaternion Rotation(VRComponent component)
		{
			if(VRComponent.LEFT_EYE == component)
			{
				return SteamVR.instance.eyes[0].rot;
			}
			return SteamVR.instance.eyes[1].rot;
		}

		public Transform leftCamera_LEGACY_HACK()
		{
			throw new System.NotImplementedException();
		}

		public Transform rightCamera_LEGACY_HACK()
		{
			throw new System.NotImplementedException();
		}
	}

}

