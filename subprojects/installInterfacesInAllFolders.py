#!/usr/bin/env python

#NOTE NOTE NOTE: on windows if using a POSIX-style path for Unity, this will fail :(
#Hours lost to this: 2
#working windows output example "D:\Program Files\Unity\Editor\Unity.exe" -projectPath "D:\dev\videoplayer\subprojects\utilities_and_scripts" -quit -batchmode -nographics -importPackage "D:\dev\videoplayer\packages\Interfaces.unitypackage"

from os import listdir
from os.path import isfile, isdir, join
import os, sys
import time
from time import sleep
from subprocess import call
import traceback
import shutil
from shutil import copytree
import sys
import platform
import posixpath


unity = sys.argv[1]
print unity

CurrentDir = os.path.dirname(os.path.realpath(__file__))
#CurrentDirPosix = posixpath.dirname(posixpath.realpath(__file__))
ProjectRoot = os.path.dirname(__file__)

if "Windows" in platform.system():
    ProjectRoot = ProjectRoot.replace("/", "\\")
    #desired result so that os.listdir works is D:/dev/videoplayer/subprojects/utilities_and_scripts
    #ProjectRoot = "/" + ProjectRoot.replace(":", "")

#ProjectRootPosix = posixpath.dirname(__file__)  # ala D:/dev/videoplayer/subprojects/utilities_and_scripts

#ProjectRoot=os.path.join(ProjectRoot,os.pardir)
ProjectRoot=os.path.abspath( os.path.join(ProjectRoot,os.pardir))
#ProjectRootPosix =ProjectRoot #this seems wrong
#print " lala" + posixpath.dirname(__file__)
import json
print "Projectroot:"+ProjectRoot + json.dumps(os.listdir(ProjectRoot))
#print "PosixProjectroot:"+ProjectRootPosix


SubProjectsDir = os.path.join(ProjectRoot, "subprojects")
print "subprojects %s projectroot %s" % (SubProjectsDir,ProjectRoot)
#SubProjectsDirPosix = posixpath.join(ProjectRootPosix, "subprojects")
#PackagesProjectDirPosix = posixpath.join(ProjectRootPosix, "packages")  # posixpath.realpath(  )

def getAllFilesInFolder(folder):
    files = []
    try:
        files = os.listdir(folder)
        files = [os.path.join(folder,s) for s in files]
    except:
        print "no files in folder: " + folder
    return files

# todo: also in levels folder
levelsDir = os.path.join(SubProjectsDir, "levels")
print levelsDir + "  vs subprojectsdir  " + SubProjectsDir
levels = getAllFilesInFolder(levelsDir)
print json.dumps(levels)
subprojects = getAllFilesInFolder(SubProjectsDir)
print json.dumps(subprojects)

foldersToScan = levels + subprojects

#foldersToScan = os.listdir(SubProjectsDir)
#foldersToScan = [os.path.join(SubProjectsDir,s) for s in foldersToScan]

#a = ['hello{0}'.format(i) for i in a]
#a = map(lambda x: 'hello%i' % x, a)

#['hello{0}'.format(i) for i in testme]
#["hello" + str(s) for s in testme]


if(os.path.isdir(os.path.join(ProjectRoot,"mainproject"))):
    foldersToScan.append(os.path.join(ProjectRoot,"mainproject"))

print json.dumps(foldersToScan)

def callUnity(packagePath, targetProjectPath):
    #executionString = '/Volumes/1tbhdd/Applications/Unity461f1/Unity.app/Contents/MacOS/Unity -projectPath /Volumes/1tbhdd/Users/FromOtherDrive/dev/ggj15/MainProject -quit -batchmode -importPackage /Volumes/1tbhdd/Users/FromOtherDrive/dev/ggj15/subProjects/Analytics.unitypackage'
    executionString = "\"%s\" -projectPath \"%s\" -quit -batchmode -nographics -importPackage \"%s\"" % \
                      (unity, targetProjectPath, packagePath )

    #call("echo 'hello world'",shell=True)
    print "importing " + packagePath + " \n"
    print executionString + "\n"
    call(executionString, shell=True)
    sleep(0.1)
    sys.stdout.flush()


finalFolders = []
for file in foldersToScan:  #os.walk("."):
    #ignore interfaces source
    if ("InterfacesSource".lower() in file.lower()):
        foldersToScan.remove(file)
        continue
    if os.path.isdir(file) is False:
        foldersToScan.remove(file)
        continue
    finalFolders.append(file)
    shutil.rmtree(os.path.join(file,"Assets","Interfaces"),True);

for file in finalFolders:
    callUnity(os.path.join(ProjectRoot,"packages", "Interfaces.unitypackage"), file)



