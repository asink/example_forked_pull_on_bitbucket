﻿using System;
using System.Collections.Generic;
using interfaces;
using UnityEngine;

//from version 0.5.0 mobile (should be smooth sailing from here on out?)

namespace implementations
{
	[Implements(typeof (IVR))]
	public class OculusVR : IVR
	{
		private readonly CameraLoader _loader;
		private readonly List<CameraCommands> _supportedCommands;

		private readonly Dictionary<VRComponent, Transform> _vrTransforms = new Dictionary<VRComponent, Transform>
		{
			{VRComponent.LEFT_EYE, null},
			{VRComponent.RIGHT_EYE, null},
			{VRComponent.CENTER, null}
		};

		private Transform _leftEye;
		private Transform _rightEye;

		public OculusVR()
		{
			_loader = new CameraLoader("OculusVR");
			_supportedCommands = new List<CameraCommands>
			{
				CameraCommands.DISMISS_HSW,
				CameraCommands.EYE_AA_ONE,
				CameraCommands.EYE_AA_TWO,
				CameraCommands.EYE_TEXTURE_DEPTH_16,
				CameraCommands.EYE_TEXTURE_DEPTH_24,
				CameraCommands.RESET_TRACKING,
				CameraCommands.STANDARD_PERFORMANCE,
				CameraCommands.THROTTLE_DOWN,
				CameraCommands.THROTTLE_DOWN_GPU,
				CameraCommands.THROTTLE_DOWN_CPU
			};
		}

		public GameObject CreateCamera()
		{
			return _loader.CreateCamera();
		}

		public CameraSettingsSO GetCameraSettingsSO()
		{
			return _loader.CameraSettings;
		}

		public void CameraCommand(CameraCommands command)
		{
			switch (command)
			{
				case CameraCommands.DISMISS_HSW:
					OVRManager.DismissHSWDisplay();
					break;
				case CameraCommands.EYE_AA_ONE:
					OVRManager.instance.eyeTextureAntiAliasing = OVRManager.RenderTextureAntiAliasing._1;
					break;
				case CameraCommands.EYE_AA_TWO:
					OVRManager.instance.eyeTextureAntiAliasing = OVRManager.RenderTextureAntiAliasing._2;
					break;
				case CameraCommands.EYE_TEXTURE_DEPTH_16:
					OVRManager.instance.eyeTextureDepth = OVRManager.RenderTextureDepth._16;
					break;
				case CameraCommands.EYE_TEXTURE_DEPTH_24:
					OVRManager.instance.eyeTextureDepth = OVRManager.RenderTextureDepth._24;
					break;
				case CameraCommands.RESET_TRACKING:
					OVRManager.DismissHSWDisplay();
					break;
				case CameraCommands.STANDARD_PERFORMANCE:
#if (UNITY_ANDROID && !UNITY_EDITOR)
			OVRModeParms.OVR_VrModeParms_SetCpuLevel(2);
			OVRModeParms.OVR_VrModeParms_SetGpuLevel(2);
			OVRPluginEvent.Issue(RenderEventType.ResetVrModeParms);
#endif
					break;
				case CameraCommands.THROTTLE_DOWN:
#if (UNITY_ANDROID && !UNITY_EDITOR)
			OVRModeParms.OVR_VrModeParms_SetCpuLevel(0);
			OVRModeParms.OVR_VrModeParms_SetGpuLevel(0);
			OVRPluginEvent.Issue(RenderEventType.ResetVrModeParms);
#endif
					break;
				case CameraCommands.THROTTLE_DOWN_CPU:
#if (UNITY_ANDROID && !UNITY_EDITOR)
			OVRModeParms.OVR_VrModeParms_SetCpuLevel(0);
			OVRPluginEvent.Issue(RenderEventType.ResetVrModeParms);
#endif
					break;
				case CameraCommands.THROTTLE_DOWN_GPU:
#if (UNITY_ANDROID && !UNITY_EDITOR)
			OVRModeParms.OVR_VrModeParms_SetGpuLevel(0);
			OVRPluginEvent.Issue(RenderEventType.ResetVrModeParms);
#endif
					break;
				default:
					throw new NotSupportedException("Command not supported:" + command);
			}
		}

		public void SetCustomCameraSettings(string settingString)
		{
			//if(settingString.ToLower().Contains("flipy"))
			//	_loader.CameraRef.GetComponent<OVRCameraRig>().FlipCorrectionInY = true;
		}

		public List<CameraCommands> GetSupportedCommands()
		{
			return _supportedCommands;
		}

		public Vector3 Position(VRComponent comp)
		{
			return getComponent(comp).position;
		}

		public Quaternion Rotation(VRComponent comp)
		{
			return getComponent(comp).rotation;
		}

		public GameObject FindChildRecursive(GameObject obj, string nameToFind)
		{
			if (obj.name == nameToFind)
				return obj;
			foreach (Transform child in obj.transform)
			{
				var foundCHild = FindChildRecursive(child.gameObject, nameToFind);
				if (foundCHild != null)
				{
					return foundCHild;
				}
			}
			return null;
		}

		private Transform getComponent(VRComponent component)
		{
			if (_vrTransforms[component] != null) return _vrTransforms[component];
			Transform newTransform = null;

			findObjectCustom gameObjectFinder = (string childName) => FindChildRecursive(_loader.CameraRef, childName).transform;
			switch (component)
			{
				case VRComponent.LEFT_EYE:
					newTransform = gameObjectFinder("LeftEyeAnchor");
					break;
				case VRComponent.RIGHT_EYE:
					newTransform = gameObjectFinder("RightEyeAnchor");
					break;
				case VRComponent.CENTER:
					newTransform = gameObjectFinder("CenterEyeAnchor");
					break;
				default:
					throw new NotSupportedException("tried to get an unrecognized vr position:" + component);
			}
			_vrTransforms[component] = newTransform;
			return newTransform;
		}

		public Transform leftCamera_LEGACY_HACK()
		{
			return getComponent(VRComponent.LEFT_EYE).transform;
		}

		public Transform rightCamera_LEGACY_HACK()
		{
			return getComponent(VRComponent.RIGHT_EYE).transform;
		}
		private delegate Transform findObjectCustom(string childName);
	}
}